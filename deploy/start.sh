#!/bin/bash

set -e

keytool -import \
  -alias cluster-ca \
  -file /cluster-ca/ca.crt \
  -keystore "$JAVA_HOME/lib/security/cacerts" \
  -noprompt \
  -storepass changeit

java -jar /app/tolkien-ai-server.jar